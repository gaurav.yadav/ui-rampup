(function () {
    var PHONE_ERROR='Phone number has to be 10 to 12 digits long.';
    var tabs = document.getElementsByClassName("tab-button");
    var navlinks = document.getElementsByClassName("navbar-links");
    var form_elements = document.getElementsByTagName("INPUT");
    function activate(event, given_class) {
        items=document.getElementsByClassName(given_class);
        for(i=0;i<items.length;i++){
            if(items[i].classList.contains("active")){
                items[i].classList.remove("active"); 
            }
        }
        event.currentTarget.classList.add("active");
    }
    function validate(){
        phone_valid=validate_phone(document.getElementById("contact-phone").value);
        email_valid=validate_email(document.getElementById("contact-email").value);
        if (document.getElementById("contact-name").value.length   >   0   &&
            email_valid  &&
            phone_valid) {
            document.getElementById("send").disabled = false;
        }
        else {
            document.getElementById("send").disabled = true;
            if (!phone_valid){
            document.getElementById("phone-error").innerHTML=PHONE_ERROR;
            }
            else{
                document.getElementById('phone-error').innerHTML=''
            }
        }
    }
    function validate_phone(phone){
        return phone.match(/^\d{10,12}$/);
    }
    function validate_email(email){
        return email.match(/^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,6}$/)
    }
    for(i=0;i<tabs.length;i++){
        tabs[i].addEventListener("click", function(){
            activate(event, 'tab-button');
        })
    }
    for(i=0;i<navlinks.length;i++){
            navlinks[i].addEventListener("click", function(){
            activate(event, 'navbar-links');
        })
    }
    for(i=0;i<form_elements.length;i++){
            form_elements[i].addEventListener("keyup", function(){
            validate();
        })
    }
}());
